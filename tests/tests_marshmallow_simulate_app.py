import json

from marshmallow import fields

from django_dynamodel.utils import TypedSchema
from .utils import _


class Address:
    def __init__(self, street: str, city: str):
        self.street: str = street
        self.city: str = city


class Company:
    def __init__(self, name: str, address: Address):
        self.name: str = name
        self.address: Address = address


class AddressSchema(TypedSchema):
    MODEL_CLASS = Address

    street = fields.Str(required=True)
    city = fields.Str(required=True)


class CompanySchema(TypedSchema):
    MODEL_CLASS = Company

    name = fields.Str(required=True)
    address = fields.Nested(AddressSchema, required=True)


def test_read_data_from_db():
    # create objects from json
    company_json_str = '{"address": {"city": "NY", "street": "5th Avenue 123"}, "name": "Startup Inc."}'
    company_json_dict = json.loads(company_json_str)
    company_obj: Company = CompanySchema().load(company_json_dict)

    # update objects
    company_obj.name = company_obj.name + ' MOD'
    company_obj.address.city = company_obj.address.city + ' MOD'

    # validate & serialize objects
    company_json_dict_updated = CompanySchema().dump(company_obj)
    assert isinstance(company_json_dict_updated, dict)
    assert _(CompanySchema().validate(company_json_dict_updated)) == _({})

    company_json_str_updated = json.dumps(company_json_dict_updated, sort_keys=True)
    assert company_json_str_updated == \
           '{"address": {"city": "NY MOD", "street": "5th Avenue 123"}, "name": "Startup Inc. MOD"}'
