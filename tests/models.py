import dataclasses
import typing

from django.contrib.postgres.fields import JSONField
from django.db import models
from marshmallow import fields

from django_dynamodel import models as dynamodels
from django_dynamodel.utils import TypedSchema


# ---------- marshmallow models (plain python models in fact)

@dataclasses.dataclass
class Tag:
    name: str


@dataclasses.dataclass
class Address:
    street: str
    city: str
    tags: typing.List[Tag] = dataclasses.field(default_factory=list)


# ---------- marshmallow schemas

class TagSchema(TypedSchema):
    MODEL_CLASS = Tag
    name = fields.Str(required=True)


class AddressSchema(TypedSchema):
    MODEL_CLASS = Address
    street = fields.Str(required=True)
    city = fields.Str(required=True)
    tags = fields.List(fields.Nested(TagSchema), required=False)


# ---------- django models

class Company(models.Model):
    name = models.CharField(max_length=30)
    json_field = JSONField(blank=True)
    address: Address = dynamodels.MarshmallowJSONField(marshmallow_schema=AddressSchema, null=True, blank=True)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('company-list')

# ---------- django forms

# class CompanyForm(ModelForm):
#     class Meta:
#         model = Company
#         fields = ['name', 'json_field', 'address']
