from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from tests.models import Company


class CompanyList(ListView):
    model = Company


class CompanyCreate(CreateView):
    model = Company
    fields = ['name', 'json_field', 'address']


class CompanyUpdate(UpdateView):
    model = Company
    fields = ['name', 'json_field', 'address']


class CompanyDelete(DeleteView):
    model = Company
    success_url = reverse_lazy('company-list')
