import typing
import dataclasses
import typing

from marshmallow import fields

from django_dynamodel.utils import TypedSchema
from .utils import _


@dataclasses.dataclass
class Tag:
    name: str


@dataclasses.dataclass
class Address:
    street: str
    city: str
    tags: typing.List[Tag] = dataclasses.field(default_factory=list)


@dataclasses.dataclass
class Company:
    name: str
    address: Address


class TagSchema(TypedSchema):
    MODEL_CLASS = Tag

    name = fields.Str(required=True)


class AddressSchema(TypedSchema):
    MODEL_CLASS = Address

    street = fields.Str(required=True)
    city = fields.Str(required=True)
    tags = fields.List(fields.Nested(TagSchema), required=False)


class CompanySchema(TypedSchema):
    MODEL_CLASS = Company

    name = fields.Str(required=True)
    address = fields.Nested(AddressSchema, required=True)


def test_serialize_company():
    company_dict = CompanySchema().dump(Company(name='Startup Inc.',
                                                address=Address(street='5th Avenue 123',
                                                                city='NY',
                                                                tags=[])))

    assert _(company_dict) == _(dict(name='Startup Inc.',
                                     address=dict(street='5th Avenue 123',
                                                  city='NY',
                                                  tags=[])))

    assert _(CompanySchema().validate(company_dict)) == _({})


def test_materialize_customer():
    # --- Customer1 / CustomerSchema
    company_dict = dict(name='Startup Inc.',
                        address=dict(street='5th Avenue 123',
                                     city='NY',
                                     tags=[dict(name='confirmed'),
                                           dict(name='international')]))
    company_obj = CompanySchema().load(company_dict)
    assert isinstance(company_obj, Company)
    assert company_obj.name == 'Startup Inc.'

    assert isinstance(company_obj.address, Address)
    assert company_obj.address.street == '5th Avenue 123'
    assert company_obj.address.city == 'NY'
    assert 'confirmed' in [tag.name for tag in company_obj.address.tags]
    assert 'international' in [tag.name for tag in company_obj.address.tags]
