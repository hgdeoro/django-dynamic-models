# Serialization libraries

By popularity:

### marshmallow

A lightweight library for converting complex objects to and from
simple Python datatypes.

* https://github.com/marshmallow-code/marshmallow
   * w: 88 | s: 4800 | f: 500

### schematics

* https://github.com/schematics/schematics
   * w: 65 | s: 2400 | f: 285

### alecthomas/voluptuous

* https://github.com/alecthomas/voluptuous
   * w: 42 | s: 1600 | f: 184

### Pylons/colander

* https://github.com/Pylons/colander
   * w: 31 | s: 388 | f: 148

### encode/typesystem

* https://github.com/encode/typesystem - https://www.encode.io/typesystem/
   * w: 29 | s: 366 | f: 30
