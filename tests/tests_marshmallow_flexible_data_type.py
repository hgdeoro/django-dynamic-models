from dataclasses import dataclass

from marshmallow import fields

from django_dynamodel.utils import TypedSchema
from .utils import _


@dataclass
class Flexible:
    data: dict


class FlexibleSchema(TypedSchema):
    MODEL_CLASS = Flexible
    data = fields.Dict()


def test_serialize():
    data = dict(
        key1='value1',
        key2=123,
        key3=['1', '2', '3'],
        key4=dict(
            sub1='sub1',
            sub2='sub2',
        ),
    )
    customer_dict = FlexibleSchema().dump(Flexible(data=data))
    assert _(customer_dict) == _(dict(data=data))
    assert _(FlexibleSchema().validate(customer_dict)) == _({})


def test_materialize_customer():
    data = dict(
        key1='value1',
        key2=123,
        key3=['1', '2', '3'],
        key4=dict(
            sub1='sub1',
            sub2='sub2',
        ),
    )
    flexible_dict = dict(data=data)
    flexible_obj = FlexibleSchema().load(flexible_dict)

    assert isinstance(flexible_obj, Flexible)
    assert isinstance(flexible_obj.data, dict)
    assert flexible_obj.data['key1'] == 'value1'
    assert flexible_obj.data['key4']['sub2'] == 'sub2'
