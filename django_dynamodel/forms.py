import logging
from typing import TypeVar

from django.contrib.postgres.forms.jsonb import JSONField as JSONFormField

from django_dynamodel import utils

T = TypeVar('T')


class MarshmallowJSONFormField(JSONFormField, utils.MarshmallowUtilsMixin):
    logger = logging.getLogger(__name__).getChild('MarshmallowJSONFormField')

    def __init__(self, marshmallow_schema: utils.TypedSchema[T], *args, **kwargs):
        self.marshmallow_schema: utils.TypedSchema[T] = marshmallow_schema
        super().__init__(*args, **kwargs)

    # FIXME: do we need to implement `to_python()`?

    # FIXME: do we need to implement `bound_data()`?

    # FIXME: do we need to implement `has_changed()`?

    # FIXME: make sure we don't need to implement any other method

    def prepare_value(self, value):
        if isinstance(value, self.marshmallow_schema.MODEL_CLASS):
            value_serialized = self._marshmallow_serialize(value)
            value = value_serialized

        return super().prepare_value(value)
