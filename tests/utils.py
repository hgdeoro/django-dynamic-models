import json


def _(data: dict) -> str:
    """Serializes dict in a predictable way (to be used in `assert`)"""
    assert isinstance(data, dict), f"The recieved object is NOT a dict, is a {type(data)}"
    return json.dumps(data, sort_keys=True, indent=2)
