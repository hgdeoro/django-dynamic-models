import pytest
from marshmallow import INCLUDE
from marshmallow import Schema
from marshmallow import ValidationError
from marshmallow import fields

from django_dynamodel.utils import TypedSchema
from .utils import _


# --- models

class Customer1:
    def __init__(self, name):
        self.name = name
        # self.address = address
        # self.extra_attr = extra_attr


class Customer2:
    def __init__(self, name, extra_attr):
        self.name = name
        # self.address = address
        self.extra_attr = extra_attr


class Customer3:
    def __init__(self, name, address=None):
        self.name = name
        self.address = address
        # self.extra_attr = extra_attr


class Customer4:
    def __init__(self, address):
        # self.name = name
        self.address = address
        # self.extra_attr = extra_attr


# --- schemas

class CustomerPlainSchema(Schema):
    name = fields.Str(required=True)
    address = fields.Str(required=False)


class CustomerTypedSchema(TypedSchema):
    name = fields.Str(required=True)
    address = fields.Str(required=False)


class Customer1TypedSchema(CustomerTypedSchema):
    MODEL_CLASS = Customer1


class Customer2TypedSchema(CustomerTypedSchema):
    MODEL_CLASS = Customer2


class Customer3TypedSchema(CustomerTypedSchema):
    MODEL_CLASS = Customer3


class Customer4TypedSchema(CustomerTypedSchema):
    MODEL_CLASS = Customer4


class Customer2PlainSchemaUnknownInclude(CustomerPlainSchema):
    class Meta:
        unknown = INCLUDE


# --- tests

def test_serialize_customer():
    # --- Customer1
    customer_dict = Customer1TypedSchema().dump(Customer1(name='John Doe'))
    assert _(customer_dict) == _(dict(name='John Doe'))
    assert _(Customer1TypedSchema().validate(customer_dict)) == _({})

    # --- Customer2
    customer_dict = Customer2TypedSchema().dump(Customer2(name='John Doe', extra_attr='Some Extra value'))
    assert _(customer_dict) == _(dict(name='John Doe'))
    assert _(Customer2TypedSchema().validate(customer_dict)) == _({})

    # --- Customer3
    customer_dict = Customer3TypedSchema().dump(Customer3(name='John Doe', address='5th Avenue 123'))
    assert _(customer_dict) == _(dict(name='John Doe', address='5th Avenue 123'))
    assert _(Customer3TypedSchema().validate(customer_dict)) == _({})

    # --- Customer4
    customer_dict = Customer4TypedSchema().dump(Customer4(address='5th Avenue 123'))
    assert _(customer_dict) == _(dict(address='5th Avenue 123'))
    assert _(Customer4TypedSchema().validate(customer_dict)) == _({
        "name": [
            "Missing data for required field."
        ]
    })


def test_materialize_customer():
    # --- Customer1 / CustomerPlainSchema
    customer_dict = dict(name='John Doe')
    customer_obj = CustomerPlainSchema().load(customer_dict)
    assert _(customer_obj) == _(dict(name='John Doe'))

    # --- Customer1 / Customer1TypedSchema
    customer_dict = dict(name='John Doe')
    customer_obj = Customer1TypedSchema().load(customer_dict)
    assert isinstance(customer_obj, Customer1)
    assert customer_obj.name == 'John Doe'

    # --- Customer2 / CustomerPlainSchema
    customer_dict = dict(name='John Doe', extra_attr='Some Extra value')
    with pytest.raises(ValidationError) as exc_info:
        customer_obj = CustomerPlainSchema().load(customer_dict)

    # NODE: Customer2TypedSchema().load(data) is VERY STRICT, it fails if `data` has extra keys
    assert _(exc_info.value.normalized_messages()) == _(dict(extra_attr=["Unknown field."]))

    # --- Customer2 / CustomerSchemaUnknownInclude
    customer_dict = dict(name='John Doe', extra_attr='Some Extra value')
    customer_obj = Customer2PlainSchemaUnknownInclude().load(customer_dict)
    assert _(customer_obj) == _(dict(name='John Doe', extra_attr='Some Extra value'))

    # --- Customer3 / CustomerPlainSchema
    customer_dict = dict(name='John Doe', address='5th Avenue 123')
    customer_obj = CustomerPlainSchema().load(customer_dict)
    assert _(customer_obj) == _(dict(name='John Doe', address='5th Avenue 123'))

    # --- Customer3 / Customer3TypedSchema / with address
    customer_dict = dict(name='John Doe', address='5th Avenue 123')
    customer_obj = Customer3TypedSchema().load(customer_dict)
    assert isinstance(customer_obj, Customer3)
    assert customer_obj.name == 'John Doe'
    assert customer_obj.address == '5th Avenue 123'

    # --- Customer3 / Customer3TypedSchema / without address
    customer_dict = dict(name='John Doe')
    customer_obj = Customer3TypedSchema().load(customer_dict)
    assert isinstance(customer_obj, Customer3)
    assert customer_obj.name == 'John Doe'
    assert customer_obj.address is None

    # --- Customer4 / CustomerPlainSchema
    customer_dict = dict(address='5th Avenue 123')

    with pytest.raises(ValidationError) as exc_info:
        customer_obj = CustomerPlainSchema().load(customer_dict)

    # NODE: attribute `name` is required
    assert _(exc_info.value.normalized_messages()) == _(dict(name=["Missing data for required field."]))


def test_serialize_boolean_on_str_field():
    # name is mandatory
    customer_obj = Customer1(name=None)
    customer_dict = Customer1TypedSchema().dump(customer_obj)
    errors = Customer1TypedSchema().validate(customer_dict)
    assert _(errors) == _(dict(name=["Field may not be null."]))

    # name is not a stirng, but it is automatically transformed to a str when dump()'ed
    # I don't like that, and I would prefer to "automatic type transformation" to be optional
    customer_obj = Customer1(name=123)
    customer_dict = Customer1TypedSchema().dump(customer_obj)
    errors = Customer1TypedSchema().validate(customer_dict)
    assert _(errors) == _({})
    assert _(customer_dict) == _(dict(name='123'))
