import pytest
from django.core.exceptions import ValidationError

from tests.models import Address
from tests.models import Company
from tests.models import Tag
from .utils import _


@pytest.mark.django_db
class TestOnlyDjangoFields:

    def test_objects_all(self):
        assert len(Company.objects.all()) == 0

    def test_company_instance_not_using_marshmallow_field(self):
        company = Company()
        company.name = "Startup Inc."
        company.json_field = [
            {
                "_id": "5ee746c798f15167db262e33",
                "index": 0,
                "guid": "da55dae3-7b97-4ae1-8fd5-275f58cbafeb",
                "isActive": True,
                "balance": "$3,994.13"
            },
            {
                "_id": "5ee746c7f26fbc9c201eb0ef",
                "index": 1,
                "guid": "1002543f-0928-47e9-8e65-8f2cd55fb574",
                "isActive": True,
                "balance": "$3,749.49"
            },
            {
                "_id": "5ee746c79b8637217d6197e2",
                "index": 2,
                "guid": "545a0893-e620-4fac-96cd-2e757ace843b",
                "isActive": True,
                "balance": "$1,044.59"
            }
        ]
        company.full_clean()
        company.save()
        assert len(list(Company.objects.all())) == 1

        company = list(Company.objects.all())[0]
        assert company.name == "Startup Inc."
        assert len(company.json_field) == 3


@pytest.mark.django_db
class TestMarshmallowJSONFieldWithValidData:

    def test_company_instance_using_marshmallow_field(self):
        company = Company()
        company.name = "Startup Inc."
        company.json_field = {}
        company.address = Address(
            street='5th Avenue 123',
            city='NY',
            tags=[Tag(name='confirmed'),
                  Tag(name='international')]
        )
        company.full_clean()
        company.save()
        assert len(list(Company.objects.all())) == 1

        company = list(Company.objects.all())[0]
        assert company.name == "Startup Inc."

        assert isinstance(company.address, Address)
        assert company.address.street == '5th Avenue 123'
        assert company.address.city == 'NY'
        assert 'confirmed' in [tag.name for tag in company.address.tags]
        assert 'international' in [tag.name for tag in company.address.tags]


@pytest.mark.django_db
class TestMarshmallowJSONFieldWithInvalidUserData:

    def test_type_conversion(self):
        company = Company()
        company.name = "Startup Inc."
        company.json_field = {}
        company.address = Address(
            street='5th Avenue 123',
            city=True,
            tags=[]
        )
        assert company.address.city is True
        company.full_clean()
        company.save()
        company = Company.objects.get(pk=company.pk)
        assert company.name == "Startup Inc."
        assert company.address.city == 'True'
        #                              ^^^^^^ was converted to str
        # As part of Schema.dump(), marshmallow converts the data types
        # In the previous code, we can see how boolean was converted to string
        # This is not good, but it's impossible to change marshmallow behaviour

    def test_missing_data(self):
        company = Company()
        company.name = "Startup Inc."
        company.json_field = {}
        company.address = Address(
            street=None,
            city='NY',
        )

        with pytest.raises(ValidationError) as exc_info:
            company.full_clean()

        assert exc_info.value
        assert len(exc_info.value.error_dict) == 1
        assert 'address' in exc_info.value.error_dict
        assert len(exc_info.value.error_dict['address']) == 1

        validation_err = exc_info.value.error_dict['address'][0]
        marshmallow_errors = validation_err.params
        assert _(marshmallow_errors) == _(dict(street=['Field may not be null.']))
