Django-DynaModel
================

Provides ‘Dynamic Models’, a way to store structured objects in a single
attribute. Since it uses a single attribute, no migration are created if
the schema changes. Objects are serialized to JSON.

This project exists to provide a better alternative to ``JSONField``:

* values are validated (you must call ``obj.full_clean()``)
* you deal with real objects instead of ``dicts`` and ``lists``
* IDE provides feedback if data types and objects attributes

Currently, the major drawback is that **marshmallow** automatically
converts data types instead of raising an exception.

Example
=======

Let’s assume we have an application, we need to store information about
a ``Company`` and its ``Address``. But we know ``Address`` will change a
lot, we don’t want to deal with migrations for that, and we want to
avoid using a plain JSON.

For **address** we need **street**, **city** and a **list of tags**.

Django model
------------

We have a ``Company`` model, the ``address`` field is
``MarshmallowJSONField``.

We need to specify the schema of the address (``AddressSchema``).

::

       class Company(models.Model):
           name = models.CharField(max_length=30)
           address: Address = dynamodels.MarshmallowJSONField(
               marshmallow_schema=AddressSchema, null=True, blank=True)


Schemas (marshmallow)
---------------------

We define the schema of ``Address`` and ``Tag``, this is needed for
validation and serialization.

::

       class TagSchema(TypedSchema):
           MODEL_CLASS = Tag
           name = fields.Str(required=True)


       class AddressSchema(TypedSchema):
           MODEL_CLASS = Address
           street = fields.Str(required=True)
           city = fields.Str(required=True)
           tags = fields.List(fields.Nested(TagSchema), required=False)

Those are **marshmallow** schemas:
https://marshmallow.readthedocs.io/en/stable/index.html

Dynamic model (plain python objects)
------------------------------------

In the application, we’ll use instances of these classes to hold
information of addresses.

::

       @dataclasses.dataclass
       class Tag:
           name: str


       @dataclasses.dataclass
       class Address:
           street: str
           city: str
           tags: typing.List[Tag] = dataclasses.field(default_factory=list)

Use of model + dynamic model
----------------------------

Create a company
~~~~~~~~~~~~~~~~

::

       company = Company()
       company.name = "Startup Inc."
       company.address = Address(
           street='5th Avenue 123',
           city='NY',
           tags=[Tag(name='confirmed'),
                 Tag(name='international')]
       )
       company.full_clean()
       company.save()

Assert values are read from DB
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Get the object from DB:

::

       company = Company.objects.get(pk=company.pk)

Check ``company.address`` is an ``Address`` and the values

::

       assert isinstance(company.address, Address)
       assert company.address.street == '5th Avenue 123'
       assert company.address.city == 'NY'

Check ``tags``:

::

       assert 'confirmed' in [tag.name for tag in company.address.tags]
       assert 'international' in [tag.name for tag in company.address.tags]


Next steps & ideas
================

* Review `MarshmallowJSONField` implementation (current implementation works
and can be used in ModelForms, but there are some `FIXME`s to check).
* Make sure we can query the field using standard Django's capabilities
(see: https://docs.djangoproject.com/en/3.0/ref/contrib/postgres/fields/#querying-jsonfield)
