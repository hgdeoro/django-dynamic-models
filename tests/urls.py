from django.urls import path
from tests import views

urlpatterns = [
    path('company/list/', views.CompanyList.as_view(), name='company-list'),
    path('company/add/', views.CompanyCreate.as_view(), name='company-add'),
    path('company/<int:pk>/', views.CompanyUpdate.as_view(), name='company-update'),
    path('company/<int:pk>/delete/', views.CompanyDelete.as_view(), name='company-delete'),
]
