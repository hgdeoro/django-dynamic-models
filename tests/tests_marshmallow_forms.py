import json

import pytest
from django.test import Client

from tests.models import Company, Address, Tag


@pytest.mark.django_db
class TestDjangoForm:

    def test_list_empty(self):
        client = Client()
        response = client.get('/company/list/')
        response = response.content.decode('utf-8')
        assert '|no-companies|' in response

    def test_list_company(self):
        address = Address(street='5th Avenue 123',
                          city='NY',
                          tags=[
                              Tag(name='tag-a-1'),
                              Tag(name='tag-b-2'),
                          ])
        company = Company(name="Startup Inc.", json_field={}, address=address)
        company.save()

        client = Client()
        response = client.get('/company/list/')
        response = response.content.decode('utf-8')
        assert '|no-companies|' not in response
        assert '|Startup Inc.|' in response
        assert '|5th Avenue 123|' in response
        assert '|tag-a-1|' in response
        assert '|tag-b-2|' in response

    def test_update_company_display_initial_data(self):
        address = Address(street='5th Avenue 123',
                          city='NY',
                          tags=[
                              Tag(name='tag-a-1'),
                              Tag(name='tag-b-2'),
                          ])
        company = Company(name="Startup Inc.", json_field={}, address=address)
        company.save()

        client = Client()
        response = client.get(f'/company/{company.pk}/')
        response = response.content.decode('utf-8')
        assert 'Startup Inc.' in response
        assert '5th Avenue 123' in response

    def test_update_company_post_update(self):
        address = Address(street='5th Avenue 123',
                          city='NY',
                          tags=[
                              Tag(name='tag-a-1'),
                              Tag(name='tag-b-2'),
                          ])
        company = Company(name="Startup Inc.", json_field={}, address=address)
        company.save()

        client = Client()
        response = client.post(f'/company/{company.pk}/', dict(
            name="NEW Startup Inc.",
            json_field="{}",
            address=json.dumps(dict(
                street='NEW 5th Avenue 123',
                city='NEW NY',
                tags=[dict(name='new-tag')],
            ))
        ))

        assert response.status_code == 302

        company = Company.objects.get(pk=company.pk)
        assert company.name == "NEW Startup Inc."
        assert company.address.street == 'NEW 5th Avenue 123'
        assert company.address.city == 'NEW NY'
        assert len(company.address.tags) == 1
        assert company.address.tags[0].name == 'new-tag'
