from typing import TypeVar, Generic, Type

from django.core.exceptions import ValidationError
from marshmallow import Schema, post_load

T = TypeVar('T')


class TypedSchema(Schema, Generic[T]):
    MODEL_CLASS: Type[T] = None

    @post_load
    def make_object(self, data, **kwargs) -> T:
        assert self.MODEL_CLASS, "To use TypedSchema, you must specify MODEL_CLASS"
        return self.MODEL_CLASS(**data)


class MarshmallowUtilsMixin:
    """
    Uses:
    - self.marshmallow_schema
    - self.logger
    """

    def _marshmallow_validate(self, value: dict, raise_on_error=False):
        errors = self.marshmallow_schema().validate(value)
        self.logger.debug("_marshmallow_validate(): marshmallow_schema=%s ; value=%s ; errors=%s",
                          self.marshmallow_schema.__name__, LazyValueType(value), errors)
        if raise_on_error and errors:
            items = []
            for key, error_list in errors.items():
                for an_error in error_list:
                    items.append(f"{key}: {an_error}")

            raise ValidationError(
                ' / '.join(items),
                code='invalid',
                params=errors,
            )

        return errors

    def _marshmallow_serialize(self, value: T) -> dict:
        to_return = self.marshmallow_schema().dump(value)
        self.logger.debug("_marshmallow_serialize(): value=%s ; to_return=%s",
                          LazyValueType(value), LazyValueType(to_return))
        return to_return

    def _marshmallow_deserialize(self, value: dict) -> T:
        to_return = self.marshmallow_schema().load(value)
        self.logger.debug("_marshmallow_deserialize(): value=%s ; to_return=%s",
                          LazyValueType(value), LazyValueType(to_return))
        return to_return


class LazyValueType(object):
    def __init__(self, data):
        self.data = data

    def __str__(self):
        return f"{self.data} (type={type(self.data)})"
