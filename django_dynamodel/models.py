import logging
from typing import Generic, TypeVar

from django.contrib.postgres.fields import JSONField
from django.utils.translation import gettext_lazy as _

from django_dynamodel import forms
from django_dynamodel import utils

T = TypeVar('T')


class MarshmallowJSONField(JSONField, Generic[T], utils.MarshmallowUtilsMixin):
    description = _('A JSON object deserialized and validated by marshmallow')
    logger = logging.getLogger(__name__).getChild('MarshmallowJSONField')

    def __init__(self, marshmallow_schema: utils.TypedSchema[T] = None, *args, **kwargs):
        assert marshmallow_schema is not None, "marshmallow_schema must be specified"
        self.marshmallow_schema = marshmallow_schema
        super().__init__(*args, **kwargs)

    # FIXME: implement value_to_string()

    # FIXME: make sure we don't need to implement any other method

    def formfield(self, **kwargs):
        return super().formfield(**{
            'form_class': forms.MarshmallowJSONFormField,
            'marshmallow_schema': self.marshmallow_schema,
            **kwargs,
        })

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        kwargs['marshmallow_schema'] = self.marshmallow_schema
        return name, path, args, kwargs

    def get_prep_value(self, value) -> T:
        """
        Map Python types to database
        """
        if isinstance(value, self.marshmallow_schema.MODEL_CLASS):
            # Transform object to dict, so it can be serialized by JSONField
            value_serialized = self._marshmallow_serialize(value)
            errors = self._marshmallow_validate(value_serialized)  # FIXME: should we raise an exception here?
            self.logger.debug("get_prep_value(): value=%s ; value_serialized=%s ; errors=%s",
                              utils.LazyValueType(value), utils.LazyValueType(value_serialized),
                              utils.LazyValueType(errors))
            value = value_serialized
        else:
            self.logger.warning("get_prep_value(): value is not an instance of %s ; value=%s",
                                self.marshmallow_schema.MODEL_CLASS, utils.LazyValueType(value))

        to_return = super().get_prep_value(value)
        return to_return

    def from_db_value(self, value, expression, connection):
        """
        Map database types to Python.
        We know that JSONField does not defines `from_db_value()`, no need to call super().from_db_value()
        """
        # https://docs.djangoproject.com/en/3.0/ref/models/fields/#django.db.models.Field.from_db_value
        # If present for the field subclass, from_db_value() will be called in all circumstances
        # when the data is loaded from the database, including in aggregates and values() calls.
        if isinstance(value, dict):
            self.logger.debug("from_db_value(): value=%s", utils.LazyValueType(value))
            return self._marshmallow_deserialize(value)

        self.logger.info("from_db_value() didn't received a dict: value=%s", utils.LazyValueType(value))
        return value

    def to_python(self, value):
        # https://docs.djangoproject.com/en/3.0/ref/models/fields/#django.db.models.Field.to_python
        # to_python() is called by deserialization and during the clean() method used from forms.
        # For to_python(), if anything goes wrong during value conversion,
        # you should raise a ValidationError exception.
        if isinstance(value, self.marshmallow_schema.MODEL_CLASS):
            self.logger.debug("to_python(): returning as is. value=%s", utils.LazyValueType(value))
            return value
        elif isinstance(value, dict):
            to_return = self._marshmallow_deserialize(value)
            self.logger.debug("to_python(): received value=%s ; returning to_return=%s",
                              utils.LazyValueType(value), utils.LazyValueType(to_return))
            return to_return
        else:
            to_return = super().to_python(value)
            return to_return

    def validate(self, value, model_instance):
        if isinstance(value, self.marshmallow_schema.MODEL_CLASS):
            # Validate using marshmallow
            value_dict = self._marshmallow_serialize(value)
            self._marshmallow_validate(value_dict, raise_on_error=True)

            # Call JSONField.validate(), but pass the `dict`
            super().validate(value_dict, model_instance)
        else:
            super().validate(value, model_instance)
