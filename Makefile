.PHONY: help
.ONESHELL:

VIRTUALENV_DIR := .virtualenv

SHELL = /bin/bash


help:
	@echo "See the sources"

virtualenv-update:
	set -e
	test -d $(VIRTUALENV_DIR) || virtualenv --seeder pip -p python3.7 $(VIRTUALENV_DIR)
	source $(VIRTUALENV_DIR)/bin/activate
	pip install pip-tools

pip-compile-sync:
	@set -e
	source $(VIRTUAL_ENV)/bin/activate
	pip-compile -o requirements.txt requirements.in
	pip-sync requirements.txt

pip-compile-upgrade-sync:
	@set -e
	source $(VIRTUAL_ENV)/bin/activate
	which pip-compile > /dev/null 2>&1 || pip install pip-tools || true
	pip-compile --upgrade -o requirements.txt requirements.in
	pip-sync requirements.txt

docker-compose-up:
	@set -e
	source $(VIRTUAL_ENV)/bin/activate
	docker-compose --env-file /dev/null -f tests/docker-compose.yml up -d

test:
	@set -e
	source $(VIRTUAL_ENV)/bin/activate
	export DJANGO_SETTINGS_MODULE='test_settings'
	export PYTHONPATH=$$(pwd)
	pytest -v tests/tests*.py

test-one:
	@set -e
	source $(VIRTUAL_ENV)/bin/activate
	export DJANGO_SETTINGS_MODULE='test_settings'
	export PYTHONPATH=$$(pwd)
	pytest -v $(TEST)

coverage-run:
	@set -e
	source $(VIRTUAL_ENV)/bin/activate
	export DJANGO_SETTINGS_MODULE='test_settings'
	export PYTHONPATH=$$(pwd)
	coverage run --branch --source=django_dynamodel -m pytest -v tests/tests*.py
	coverage report

coverage-html:
	coverage html --skip-empty
	xdg-open ./htmlcov/index.html

coverage: coverage-run coverage-html
